﻿using UnityEngine;

public class GameEnd : GameState
{
    public GameEnd(DealerController dealerController) : base(dealerController)
    {
    }

    public override void OnBeginning()
    {
        Debug.Log("GAME END");
    }
}