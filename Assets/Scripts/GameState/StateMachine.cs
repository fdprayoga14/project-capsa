﻿using UnityEngine;

public abstract class StateMachine : MonoBehaviour
{
    protected GameState GameState;

    public void SetState(GameState gameState)
    {
        GameState = gameState;
        GameState.OnBeginning();
    }
}