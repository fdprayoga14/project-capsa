﻿using UnityEngine;

public class GameSelectionCharacter : GameState
{
    public GameSelectionCharacter(DealerController dealerController) : base(dealerController)
    {
    }

    public override void OnBeginning()
    {
        this.PostNotification(EventMessengers.SELECTION_MODE, DealerController.DefaultState.SelectionCharacter);
    }
}