﻿using UnityEngine;
using CardSet = System.Collections.Generic.List<Card>;

public class GameBegin : GameState
{
    public GameBegin(DealerController dealerController) : base(dealerController)
    {
    }

    public override void OnBeginning()
    {
        Debug.Log("GAME STARTED! WILL DELIVER THE CARD");
        Init();
    }

    private void Init()
    {
        var allCard = new Card[52];
        for (var i = 0; i < Card.nominalOrder.Length; ++i) {
            var spade = Object.Instantiate (DealerController.cardPrefab, DealerController.defaultCardParent);
            spade.cardType = Card.CardType.Spade;
            spade.Nominal = Card.nominalOrder [i];
            allCard [i * 4 + 0] = spade;

            var heart = Object.Instantiate (DealerController.cardPrefab, DealerController.defaultCardParent);
            heart.cardType = Card.CardType.Heart;
            heart.Nominal = Card.nominalOrder [i];
            allCard [i * 4 + 1] = heart;

            var club = Object.Instantiate (DealerController.cardPrefab, DealerController.defaultCardParent);
            club.cardType = Card.CardType.Club;
            club.Nominal = Card.nominalOrder [i];
            allCard [i * 4 + 2] = club;

            var diamond = Object.Instantiate (DealerController.cardPrefab, DealerController.defaultCardParent);
            diamond.cardType = Card.CardType.Diamond;
            diamond.Nominal = Card.nominalOrder [i];
            allCard [i * 4 + 3] = diamond;
        }
        new System.Random().Acak(allCard);
        InitPlayer(allCard);
    }

    private void InitPlayer(Card[] allCard)
    {
        var set = new CardSet();
        set.AddRange(allCard);
        for (var i = 0; i < DealerController.players.Count; i++)
        {
            DealerController.players[i].id = i;
            DealerController.players[i].Cards = set.GetRange(i * 13, 13);
        }

        DealerController.SetState(new GameStarted(DealerController));
    }
}