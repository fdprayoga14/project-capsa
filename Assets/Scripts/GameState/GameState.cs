﻿using CardSet = System.Collections.Generic.List<Card>;

public abstract class GameState
{
    protected DealerController DealerController;

    public GameState(DealerController dealerController)
    {
        DealerController = dealerController;
    }
    public virtual void OnBeginning()
    {
    }
}