using UnityEngine;

public class GameStarted : GameState
{
    public GameStarted(DealerController dealerController) : base(dealerController)
    {
    }

    public override void OnBeginning()
    {
        SetFirstTurnPlayer();
    }

    private void SetFirstTurnPlayer()
    {
        for (var i = 0; i < DealerController.players.Count; i++)
        {
            var firstCard = DealerController.players[i].Cards[0];
            if (firstCard.Nominal != "3" || firstCard.cardType != Card.CardType.Diamond) continue;
            DealerController.nextTurnPlayer = DealerController.lastTurnPlayer = i;
            Debug.Log($"GAME START SET PLAYER {i + 1} WILL DO FIRST TURN!");
            if(DealerController.FirstTimePlay)
                DealerController.OnBeginTrick();
            else
                DealerController.players[i].SetTurnState(new PlayerBegin(DealerController.players[i]));
            break;
        }
    }
}