﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class DealerView : MonoBehaviour
{
    public Image detailUICardTakeOut;
    public Sprite[] detailedImageShow;


    private void OnEnable()
    {
        this.AddObserver(ShowUICard, EventMessengers.SHOW_UI_CARD);
    }

    private void OnDisable()
    {
        this.RemoveObserver(ShowUICard, EventMessengers.SHOW_UI_CARD);
    }

    private async void ShowUICard(object sender, object data)
    {
        var id = (int) data;
        
        if(id < 2) return;
        
        detailUICardTakeOut.sprite = detailedImageShow[id];
        detailUICardTakeOut.gameObject.SetActive(true);
        await Task.Delay(1100);
        detailUICardTakeOut.gameObject.SetActive(false);
    }
}