﻿using UnityEngine;
using CardSet = System.Collections.Generic.List<Card>;

public class PlayerEnd : PlayerStateBase
{
    public PlayerEnd(PlayerController _playerController) : base(_playerController)
    {
    }

    public override void OnTurnEnd()
    {
        Debug.Log($"Player {PlayerController.id + 1} END Their TURN, GO NEXT PLAYER");
        PlayerController.EndTurn();
    }
}