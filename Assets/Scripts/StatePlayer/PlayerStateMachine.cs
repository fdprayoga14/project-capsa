﻿using UnityEngine;

public abstract class PlayerStateMachine : MonoBehaviour
{
    protected PlayerStateBase PlayerState;

    public void SetTurnState(PlayerStateBase playerState)
    {
        PlayerState = playerState;
        PlayerState.OnTurnStart();
    }

    public void EndTurn(PlayerStateBase playerState)
    {
        PlayerState = playerState;
        playerState.OnTurnEnd();
    }
}