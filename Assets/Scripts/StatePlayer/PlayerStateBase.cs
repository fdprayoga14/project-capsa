using CardSet = System.Collections.Generic.List<Card>;

public abstract class PlayerStateBase
{
    protected PlayerController PlayerController;

    public PlayerStateBase(PlayerController _playerController)
    {
        PlayerController = _playerController;
    }

    public virtual void OnTurnStart()
    {
    }

    public virtual void OnTurnEnd()
    {
        
    }
}
