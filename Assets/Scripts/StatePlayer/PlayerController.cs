using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CardSet = System.Collections.Generic.List<Card>;

public class PlayerController : PlayerStateMachine
{
    public int id;
    public PlayerUiController pUI;
    [HideInInspector] public bool FirstTime;
    
    private CardSet cards;
    private List<PokerHand> hands = new List<PokerHand>(13);
    private DealerController _controller;
    private bool isPass;
    
    bool isAnalyzing = false;
    bool analyzeAllMatch = true;
    PokerHand.CombinationType analyzeCombination = PokerHand.CombinationType.Invalid;
    Func<Card, bool> analyzeFilter = null;
    
    [SerializeField]
    private bool isAi;

    private void Awake()
    {
        if (!_controller) _controller = FindObjectOfType<DealerController>();
    }

    private void Start()
    {
	    if (id != 0) isAi = true;
	    else if (_controller.fullAi && id == 0) isAi = true;
	    else isAi = false;
    }

    public CardSet Cards {
        get { return cards; }
        set {
            cards = value; 
            cards.Sort();
            pUI.TotalCard = cards.Count;
            pUI.Display(cards, isAi);
        }
    }

    public void OnTurnStart()
    {
	    var newStruct = new StructUser
	    {
		    startTurn = true,
		    playerId = id
	    };
	    this.PostNotification(EventMessengers.SEND_PLAYER_NOTIF, newStruct);
	    
	    var lastTrick = _controller.LastTrick;
        if (lastTrick != null)
        {
            analyzeCombination = lastTrick.Combination;
            analyzeFilter = trick => trick > lastTrick.Key;
        }

        if (!isAi) pUI.SetLabelSelection = true;
        StartCoroutine ("OnTurn");
        StartCoroutine ("Analyze");
    }
    IEnumerator OnTurn()
    {
	    var time = 15f;
        var lastUpdateTime = Time.time;

        while (time > 0f) {
            yield return new WaitForSeconds(0.1f);

            time -= (Time.time - lastUpdateTime);
            lastUpdateTime = Time.time;
            pUI.SetTimer = time;
            if (!isAi || isAnalyzing) continue;
            if (hands.Count > 0) {
	            if (!IsInvoking("AutoDeal")) {
		            Invoke("AutoDeal", UnityEngine.Random.Range(pUI.Hint.Count * 0.5f, pUI.Hint.Count * 2f));
	            }
            } else {
	            yield return new WaitForSeconds(0.5f);
	            break;
            }
        }
        Pass();
    }
    
    public void AutoDeal() {
	    Deal (pUI.Hint);
    }
    public void Deal(CardSet dealCards) {
	    if (dealCards.Count == 0)
		    return;

	    var hand = PokerHand.Make (dealCards);
	    if (_controller.Deal(hand))
	    {
		    pUI.OnDealSuccess();
	    }
	    else
	    {
		    Debug.Log("DEAL FAILED");
	    }
    }
    public void Deal(PokerHand hand)
    {
	    if (_controller.Deal(hand))
	    {
		    pUI.OnDealSuccess();
	    }
	    else
	    {
		    Debug.Log("DEAL FAILED");
	    }
    }
    
    public bool IsPass {
	    get { return isPass; }
	    set { 
		    isPass = value;
	    }
    }
    public void EndTurn() {
	    if (!isAi) pUI.SetLabelSelection = false;
	    pUI.TurnOffTimer();
	    StopCoroutine ("Analyze");
	    StopCoroutine ("OnTurn");

	    //pUI.OnTurnEnd ();
    }
    
    public void Pass() {
	    Debug.Log($"Player_{id + 1} PASS!");
        isPass = true;
        _controller.Pass ();
    }

    IEnumerator Analyze() {
		isAnalyzing = true;
		hands.Clear ();
		List<PokerHand> result;
		
		switch (analyzeCombination) {
		case PokerHand.CombinationType.Invalid:
			goto case PokerHand.CombinationType.One;
		case PokerHand.CombinationType.One:
			hands.AddRange (One.Instance.LazyEvaluator (cards, false, analyzeFilter));

			if (analyzeCombination == PokerHand.CombinationType.Invalid)
				goto case PokerHand.CombinationType.Pair;
			break;
		case PokerHand.CombinationType.Pair:
			hands.AddRange (Pair.Instance.LazyEvaluator (cards, analyzeAllMatch, analyzeFilter));
			
			if (analyzeCombination == PokerHand.CombinationType.Invalid)
				goto case PokerHand.CombinationType.Triple;
			break;
		case PokerHand.CombinationType.Triple:
			hands.AddRange (Triple.Instance.LazyEvaluator (cards, analyzeAllMatch, analyzeFilter));
			
			if (analyzeCombination == PokerHand.CombinationType.Invalid)
				goto case PokerHand.CombinationType.Straight;
			break;
		case PokerHand.CombinationType.Straight:
			result = Straight.Instance.LazyEvaluator (cards, analyzeAllMatch, analyzeFilter);
			hands.AddRange (result);
			if (result.Count > 0)
				pUI.Straight = result[result.Count - 1].Cards;
			if (!analyzeAllMatch && hands.Count > 0)
				break;
			yield return null;
			goto case PokerHand.CombinationType.Flush;
		case PokerHand.CombinationType.Flush:
			result = Flush.Instance.LazyEvaluator (cards, analyzeAllMatch, analyzeFilter);
			hands.AddRange (result);
			if (result.Count > 0)
				pUI.Flush = result[result.Count - 1].Cards;
			if (!analyzeAllMatch && hands.Count > 0)
				break;
			yield return null;
			goto case PokerHand.CombinationType.FullHouse;
		case PokerHand.CombinationType.FullHouse:
			result = FullHouse.Instance.LazyEvaluator (cards, analyzeAllMatch, analyzeFilter);
			hands.AddRange (result);
			if (result.Count > 0)
				pUI.FullHouse = result[result.Count - 1].Cards;
			if (!analyzeAllMatch && hands.Count > 0)
				break;
			yield return null;
			goto case PokerHand.CombinationType.FourOfAKind;
		case PokerHand.CombinationType.FourOfAKind:
			result = FourOfAKind.Instance.LazyEvaluator (cards, analyzeAllMatch, analyzeFilter);
			hands.AddRange (result);
			if (result.Count > 0)
				pUI.FourOfAKind = result[result.Count - 1].Cards;
			if (!analyzeAllMatch && hands.Count > 0)
				break;
			yield return null;
			goto case PokerHand.CombinationType.StraightFlush;
		case PokerHand.CombinationType.StraightFlush:
			result = StraightFlush.Instance.LazyEvaluator (cards, analyzeAllMatch, analyzeFilter);
			hands.AddRange (result);
			if (result.Count > 0)
				pUI.StraightFlush = result[result.Count - 1].Cards;
			if (!analyzeAllMatch && hands.Count > 0)
				break;
			yield return null;
			goto case PokerHand.CombinationType.RoyalFlush;
		case PokerHand.CombinationType.RoyalFlush:
			result = RoyalFlush.Instance.LazyEvaluator (cards, analyzeAllMatch, analyzeFilter);
			hands.AddRange (result);
			if (result.Count > 0)
				pUI.RoyalFlush = result[result.Count - 1].Cards;
			if (!analyzeAllMatch && hands.Count > 0)
				break;
			yield return null;
			goto case PokerHand.CombinationType.Dragon;
		case PokerHand.CombinationType.Dragon:
			result = Dragon.Instance.LazyEvaluator (cards, analyzeAllMatch, analyzeFilter);
			hands.AddRange (result);
			if (result.Count > 0)
				pUI.Dragon = result[result.Count - 1].Cards;
			break;
		}

		if (hands.Count > 0) {
			yield return null;
			var curr = hands[0];
			if (hands[hands.Count - 1].Combination == PokerHand.CombinationType.Dragon) {
				curr = hands[hands.Count - 1];
			} else {
				for (int i = 0; i < hands.Count; ++i) {
					if (_controller.IsFirstTurn
					    && !(hands [i].Cards [0].Nominal == "3" && hands [i].Cards [0].cardType == Card.CardType.Diamond))
						continue;
					if (curr.Combination != hands[i].Combination)
						curr = hands[i];
					if (curr.Combination >= PokerHand.CombinationType.Straight)
						break;
				}
			}
			pUI.Hint = curr.Cards;
		} else {
			pUI.Hint = null;
		}

		isAnalyzing = false;
		analyzeFilter = null;
		analyzeCombination = PokerHand.CombinationType.Invalid;
	}
}
