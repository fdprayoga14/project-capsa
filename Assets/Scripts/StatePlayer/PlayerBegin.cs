﻿using UnityEngine;
using CardSet = System.Collections.Generic.List<Card>;

public class PlayerBegin : PlayerStateBase
{
    public PlayerBegin(PlayerController _playerController) : base(_playerController)
    {
    }

    public override void OnTurnStart()
    {
        Debug.Log($"Player {PlayerController.id + 1} is Going to Start TURN");
        PlayerController.OnTurnStart();
    }
}