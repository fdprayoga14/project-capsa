public class EventMessengers
{
    public static string ON_GAME_OVER = "ON_GAME_OVER";
    public static string SHOW_UI_CARD = "SHOW_UI_CARD";
    public static string SELECTION_MODE = "SELECTION_MODE";
    public static string SET_ALL_USER_EMO = "SET_ALL_USER_EMO";
    public static string SET_USER_EMO = "SET_USER_EMO";
    public static string SEND_PLAYER_NOTIF = "SEND_PLAYER_NOTIF";
}
