using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using CardSet = System.Collections.Generic.List<Card>;

public class DealerController : StateMachine
{
    public enum DefaultState
    {
        SelectionCharacter,
        MainGame
    }

    public bool fullAi;
    [Header("Variables")]
    public Card cardPrefab;
    public Transform defaultCardParent;
    public DealerView dUI;
    public List<PlayerController> players;
    [HideInInspector] public bool FirstTimePlay = true;
    [HideInInspector]
    public int nextTurnPlayer;
    [HideInInspector] public int lastTurnPlayer;

    private List<PokerHand> tricks = new List<PokerHand>();
    private bool firstTurn = true;
    private int passPlayer;
    private void Start()
    {
        SetState(new GameSelectionCharacter(this));
    }

    private void OnEnable()
    {
        this.AddObserver(StartGame, EventMessengers.SELECTION_MODE);
    }

    private void StartGame(object arg1, object data)
    {
        var id = (int) data;
        if (id == 0) return;

        SetState(new GameBegin(this));
    }

    private void OnDisable()
    {
        this.RemoveObserver(StartGame, EventMessengers.SELECTION_MODE);
    }

    public bool IsFirstTurn => firstTurn;
    
    public int CurrentPlayer {
        get {
            return nextTurnPlayer == 0 ? players.Count - 1 : nextTurnPlayer - 1;
        }
    }
    
    void NextTurn() {
        var current = CurrentPlayer;
        players[current].EndTurn(new PlayerEnd(players[current]));

        // Stop game if one player already spent all his cards
        if (players [current].Cards.Count == 0) {
            Debug.Log($"GAME END WINNER IS PLAYER {current + 1}");
            SetState(new GameEnd(this));
            return;
        }

        while (players[nextTurnPlayer].IsPass) {
            nextTurnPlayer = nextTurnPlayer + 1 >= players.Count ? 0 : nextTurnPlayer + 1;
        }

        players[nextTurnPlayer].SetTurnState(new PlayerBegin(players[nextTurnPlayer]));
		
        nextTurnPlayer = nextTurnPlayer + 1 >= players.Count ? 0 : nextTurnPlayer + 1;
    }

    
    
    public bool Deal(PokerHand hand) {
        void TakeCards()
        {
            foreach (var t in hand.Cards)
            {
                Transform transform1;
                (transform1 = t.transform).SetParent(transform.GetChild(CurrentPlayer));
                transform1.localRotation = Quaternion.identity;
                transform1.localScale = Vector3.one;
                t.button.interactable = false;
                t.SetFrontCard();
                players[CurrentPlayer].Cards.Remove(t);
            }

            Debug.Log("Integer ID = " + (int) hand.Combination);
            this.PostNotification(EventMessengers.SHOW_UI_CARD, (int) hand.Combination);
            firstTurn = false;
        }

        var newStruct = new StructUser
        {
            playerId = CurrentPlayer
        };

        if (hand.Cards.Count == 0 || hand.Combination == PokerHand.CombinationType.Invalid)
        {
            newStruct.message = "Invalid deal !";
            return false;
        }
		
        if (firstTurn && !(hand.Cards [0].Nominal == "3" && hand.Cards [0].cardType == Card.CardType.Diamond)) {
            newStruct.message = "Must include 3 Diamond";
            return false;
        }
		
        if (tricks.Count == 0) {
            tricks.Add (hand);
            lastTurnPlayer = nextTurnPlayer == 0 ? players.Count - 1 : nextTurnPlayer - 1;
            TakeCards ();
            NextTurn ();

            newStruct.emo = "happy";
            this.PostNotification(EventMessengers.SEND_PLAYER_NOTIF, newStruct);
            return true;
        } else {
            var last = tricks[tricks.Count - 1];
            newStruct.emo = "happy";
            if (hand.Combination <= PokerHand.CombinationType.Triple && last.Combination <= PokerHand.CombinationType.Triple) {
                if (hand.Combination == last.Combination && hand.Key > last.Key) {
                    tricks.Add (hand);
                    lastTurnPlayer = nextTurnPlayer == 0 ? players.Count - 1 : nextTurnPlayer - 1;
                    TakeCards ();
                    NextTurn ();
                    this.PostNotification(EventMessengers.SEND_PLAYER_NOTIF, newStruct);
                    return true;
                }
            } else if (hand.Combination > PokerHand.CombinationType.Triple && last.Combination >  PokerHand.CombinationType.Triple) {
                if (hand > last) {
                    tricks.Add (hand);
                    lastTurnPlayer = nextTurnPlayer == 0 ? players.Count - 1 : nextTurnPlayer - 1;
                    TakeCards ();
                    NextTurn ();
                    this.PostNotification(EventMessengers.SEND_PLAYER_NOTIF, newStruct);
                    return true;
                }
            }

            newStruct.message = "Please check your card!";
            newStruct.emo = "sad";
            this.PostNotification(EventMessengers.SEND_PLAYER_NOTIF, newStruct);
            return false;
        }
    }

    public void Pass() {
        if (tricks.Count > 0) ++passPlayer;
		
        if (players.Count - passPlayer > 1)
            NextTurn ();
        else
            OnEndTrick ();
        firstTurn = false;
    }
    void OnEndTrick() {
        
        foreach (var t1 in tricks.SelectMany(t => t.Cards))
        {
            Destroy(t1.gameObject);
        }
        tricks.Clear ();

        OnBeginTrick ();
    }
    public void OnBeginTrick()
    {
        if (FirstTimePlay) FirstTimePlay = false;
        foreach (var t in players)
        {
            t.EndTurn(new PlayerEnd(t));
            t.IsPass = false;
        }
		
        passPlayer = 0;
        nextTurnPlayer = lastTurnPlayer;
		
        NextTurn ();
    }
    public PokerHand LastTrick => tricks.Count > 0 ? tricks[tricks.Count - 1] : null;
}

public class StructUser
{
    public int playerId;
    public string message;
    public string emo;
    public bool startTurn;
}
