﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class Card : MonoBehaviour, IComparable<Card>
{
    public enum CardType
    {
        Diamond,
        Club,
        Heart,
        Spade
    }
    public static string [] nominalOrder = {"3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A", "2" };
    public CardType cardType;
    public Button button;
    
    private int _score;
    private string _nominal;
    private PlayerController ownerCard;
    private Image image;
    private void Awake()
    {
        button = GetComponent<Button>();
        if(!image) image = transform.GetChild(0).GetComponent<Image> ();
    }

    public void SetFrontCard()
    {
        image.sprite = SpriteCollection.From("playingCards").Get(cardType + "_" + _nominal);
    }

    public void SetBackCard()
    {
        Debug.Log($"SET BACK CARD FOR {cardType} {_nominal}");
        image.sprite = SpriteCollection.From("playingCards").Get("Back_Card");
    }

    private PlayerController Owner
    {
        get
        {
            if (!ownerCard) ownerCard = GetComponentInParent<PlayerController>();
            return ownerCard;
        }
    }
    public int Score
    {
        get { return _score; }
    }
    public string Nominal {
        set {
            _nominal = value.ToUpper();
            _score = Array.IndexOf<string>(nominalOrder, _nominal);
        }

        get {
            return _nominal;
        }
    }
    public int CompareTo(Card other)
    {
        if (this._score == other._score)
            return (int) this.cardType - (int) other.cardType;
        else
            return this._score - other._score;
    }

    private bool onSelected;
    public void Select()
    {
        onSelected = !onSelected;
        if (onSelected) {
            image.rectTransform.anchoredPosition = new Vector2 (0, 20);
            Owner.pUI.OnCardMarked(this);
        } else {
            image.rectTransform.anchoredPosition = Vector2.zero;
            Owner.pUI.OnCardUnmarked(this);
        } 
    }
    
    public static bool operator >(Card lhs, Card rhs){
        return lhs.CompareTo(rhs) > 0;
    }
    public static bool operator <(Card lhs, Card rhs){
        return lhs.CompareTo(rhs) < 0;
    }
}