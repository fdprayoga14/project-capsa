using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MainUiController : MonoBehaviour
{
    public GameObject uiSelectionCharacter;
    public Image[] avatarPlayers;
    public TextMeshProUGUI[] playersInformation;
    public string[] imageIdStr;
    public string[] playerStrName;
    
    private List<string> playersSelectedName = new List<string>();
    
    private void OnEnable()
    {
        this.AddObserver(SelectionMode, EventMessengers.SELECTION_MODE);
        this.AddObserver(SetAllPlayerEmoticon, EventMessengers.SET_ALL_USER_EMO);
        this.AddObserver(SetPlayerEmoticon, EventMessengers.SEND_PLAYER_NOTIF);
    }

    private void OnDisable()
    {
        this.RemoveObserver(SelectionMode, EventMessengers.SELECTION_MODE);
        this.RemoveObserver(SetAllPlayerEmoticon, EventMessengers.SET_ALL_USER_EMO);
        this.RemoveObserver(SetPlayerEmoticon, EventMessengers.SEND_PLAYER_NOTIF);
    }

    public void SelectedCharacterId(int idSelected)
    {
        usedValues.Add(idSelected);
        var selectedName = playerStrName[idSelected];
        playersSelectedName.Add(selectedName);
        
        for (var i = 0; i < 3; i++)
        {
            var selectedNumber = RandomUniqueNumbers(0, 4);
            usedValues.Add(selectedNumber);
            selectedName = playerStrName[selectedNumber];
            playersSelectedName.Add(selectedName);
        }

        this.PostNotification(EventMessengers.SET_ALL_USER_EMO, "normal");
        this.PostNotification(EventMessengers.SELECTION_MODE, 1);
    }

    private void SetPlayerEmoticon(object arg1, object data)
    {
        var infoDetail = (StructUser) data;
        var emo = string.Empty;

        if (infoDetail.startTurn)
        {
            emo = playersSelectedName[infoDetail.playerId] + "_normal";
            
            avatarPlayers[infoDetail.playerId].sprite = SpriteCollection.From("Avatar").Get(emo);

            return;
        }
        
        for (var i = 0; i < avatarPlayers.Length; i++)
        {
            if (infoDetail.playerId == i)
            {
                if (infoDetail.message != string.Empty) playersInformation[i].text = infoDetail.message;
                if (infoDetail.emo != string.Empty) emo = playersSelectedName[i] + "_" + infoDetail.emo;
            }
            else
            {
                if (infoDetail.emo != string.Empty && infoDetail.message != string.Empty)
                    emo = playersSelectedName[i] + "_sad";
                else break;
            }
            
            avatarPlayers[i].sprite = SpriteCollection.From("Avatar").Get(emo);
        }
    }

    private void SetAllPlayerEmoticon(object arg1, object data)
    {
        var nameId = (string) data;
        for (var i = 0; i < avatarPlayers.Length; i++)
        {
            var emo = playersSelectedName[i] + "_" + nameId;
            avatarPlayers[i].sprite = SpriteCollection.From("Avatar").Get(emo);
        }
    }
    private void SelectionMode(object sender, object data)
    {
        var idSelection = (int) data;
        uiSelectionCharacter.SetActive(idSelection == 0);
    }

    private List<int> usedValues = new List<int>();
    private int RandomUniqueNumbers(int min, int max)
    {
        var val = Random.Range(min, max);
        while(usedValues.Contains(val))
        {
            val = Random.Range(min, max);
        }
        return val;
    }
}
