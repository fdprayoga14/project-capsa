﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using CardSet = System.Collections.Generic.List<Card>;

public class PlayerUiController : MonoBehaviour
{
	public TextMeshProUGUI labelTimer;
	public GameObject labelSelection;
	
	private CardSet straight;
	private CardSet flush;
	private CardSet fullHouse;
	private CardSet fourOfAKind;
	private CardSet straightFlush;
	private CardSet royalFlush;
	private CardSet dragon;
	private CardSet hint;
	
    private TextMeshProUGUI labelCount;
    CardSet markedCards = new CardSet();
    
    public CardSet MarkedCards => markedCards;
    PokerHand.CombinationType lastMarkCombination = PokerHand.CombinationType.Invalid;
    
    private PlayerController _controller;

    private void Awake()
    {
	    _controller = GetComponent<PlayerController>();
    }

    public bool SetLabelSelection
    {
	    set
	    {
		    if(labelSelection) labelSelection.gameObject.SetActive(value);
	    }
    }

    public void OnCardMarked(Card card) {
	    markedCards.Add (card);
	    lastMarkCombination = PokerHand.CombinationType.Invalid;
    }
    public void OnCardUnmarked(Card card) {
	    markedCards.Remove (card);
	    lastMarkCombination = PokerHand.CombinationType.Invalid;
    }
    
    public void OnDeal() {
	    _controller.Deal(markedCards);
    }

    public void OnPass()
    {
	    _controller.Pass();
    }
    
    public float SetTimer
    {
	    set
	    {
		    if (!labelTimer) return;
		    labelTimer.gameObject.SetActive(true);
		    labelTimer.text = value.ToString("F0") + " sec";
	    }
    }

    public void TurnOffTimer()
    {
	    if (labelTimer) labelTimer.gameObject.SetActive(false);
    }
    
    public CardSet Straight {
	    get { return straight; }
	    set => straight = value;
    }
	
    public CardSet Flush {
	    get { return flush; }
	    set => flush = value;
    }
	
    public CardSet FullHouse {
	    get { return fullHouse; }
	    set => fullHouse = value;
    }

    public CardSet FourOfAKind {
	    get { return flush; }
	    set => fourOfAKind = value;
    }
	
    public CardSet StraightFlush {
	    get { return straightFlush; }
	    set => straightFlush = value;
    }

    public CardSet RoyalFlush {
	    get { return royalFlush; }
	    set => royalFlush = value;
    }
	
    public CardSet Dragon {
	    get { return dragon; }
	    set => dragon = value;
    }

    public CardSet Hint {
	    get { return hint; }
	    set { hint = value; }
    }
    public int TotalCard
    {
	    set
	    {
		    if (labelCount) labelCount.text = "" + value;
	    }
    }

    public void Display(CardSet set, bool isBack)
    {
	    Debug.Log("SET PARENT");
	    foreach (var t in set)
	    {
		    t.transform.SetParent(transform, false);
		    if (isBack)
		    {
			    t.SetBackCard();
			    t.GetComponent<Button>().enabled = false;
		    }
		    else
		    {
			    t.SetFrontCard();
			    t.GetComponent<Button>().enabled = true;
		    }
	    }
    }

    public void OnDealSuccess()
    {
	    markedCards.Clear ();
	    this.TotalCard = _controller.Cards.Count;
	    Debug.Log("DEAL SUCCESS");
    }
}